<?php

declare(strict_types=1);

namespace Grifix\JwtBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class GrifixJwtBundle extends Bundle
{
}
