<?php

declare(strict_types=1);

namespace Grifix\JwtBundle\Tests;

use Grifix\Jwt\Payload;
use Grifix\JwtBundle\GrifixJwtBundle;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class BundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    /**
     * @param mixed[] $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        /** @var TestKernel $kernel */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(GrifixJwtBundle::class);
        $kernel->addTestConfig(__DIR__ . '/test_config.yaml');

        return $kernel;
    }

    public function testItWorks(): void
    {
        $kernel = self::bootKernel();

        /** @var ServiceWrapper $wrapper */
        $wrapper = $kernel->getContainer()->get(ServiceWrapper::class);
        $payload = Payload::create(['foo' => 'bar']);
        $token = $wrapper->getJwt()->encode($payload);
        self::assertEquals($payload, $wrapper->getJwt()->decode($token));
    }
}
