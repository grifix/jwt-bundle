<?php

declare(strict_types=1);

namespace Grifix\JwtBundle\Tests;

use Grifix\Jwt\JwtInterface;

final class ServiceWrapper
{
    public function __construct(private readonly JwtInterface $jwt)
    {
    }

    public function getJwt(): JwtInterface
    {
        return $this->jwt;
    }
}
